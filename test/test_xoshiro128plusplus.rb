require 'minitest/autorun'
require 'xoshiro128plusplus'

class TestXoshiro128plusplus < Minitest::Test
  def test_xoshiro128plusplus
    assert_equal 6320181, Xoshiro128plusplus.next()
  end
end
