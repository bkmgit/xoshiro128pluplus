This is an implementation of the Xoshiro128++
random number generation algorithm as a 
Ruby Gem. The algorithm is described at:

https://prng.di.unimi.it/

This is still work in progress, contributions
are welcome.

You can build and install the gem as follows:

git clone https://gitlab.com/bkmgit/xoshiro128plusplus
cd xoshiro128plusplus
rake compile
rake test
gem build xoshiro128plusplus.gemfile
gem install xoshiro128plusplus-0.0.1.gem

You can then generate random 32 bit integers in irb as follows

irb>require 'xoshiro128plusplus'
irb>Xoshiro128plusplus.next()

At present a fixed seed is used, but this will be updated to allow
the user the option of specifying the seed. 
